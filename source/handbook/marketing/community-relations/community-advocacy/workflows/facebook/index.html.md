---
layout: handbook-page-toc
title: "Facebook response workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

## Workflow

## Best practices

## Automation

Messages sent to our [Facebook page](https://www.facebook.com/gitlab/) feed into ZenDesk.
